# GitLabCI/CDの情報

GitLabにおいてCI/CD環境を実現するのに必要な情報をざっくばらんに記載するページ。

## 概要

GitLabにてCI/CDを実現するために、GitLabに組み込まれているツール  
GitLab CI/CDを使用するためには、以下2つが必要。  
・ジョブを実行できるランナーが存在することを確認する。  
　すべてのプロジェクトで利用できる共有ランナーなどが存在する場合がある。  
・`.gitlab-ci.yaml`ファイルをリポジトリのルートに作成する。  
　このファイルにジョブを定義する。  
詳細：https://docs.gitlab.com/ee/ci/quick_start/index.html

GitlabRunnerなどの語句やCI/CDの流れはこの記事を参照  
https://qiita.com/bremen/items/f47f383b9931a840a25c

## 用語集
