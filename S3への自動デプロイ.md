# S3への自動デプロイ

## AWSでの準備

### S3バケットを作成

以下を設定したバケットを作成する
- 静的ウェブサイトホスティングが有効
- パブリックアクセスを有効
- バケットポリシーを追加

詳細な設定方法は、[AWSのドキュメント](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/HostingWebsiteOnS3Setup.html)のStep1~4を参考

### S3へアップロード可能なIAMユーザーを作成

S3へアップロード可能なIAMユーザーを作成。
生成されたアクセスキーIDとシークレットアクセスキーはメモ

### GitlabCI実行時の環境変数を修正

上記で生成されたアクセスキーIDとシークレットアクセスキーをGitlabの環境変数に設定。

### GitlabCIの作成

以下を追加

```
deploy:
  image: python:alpine
  stage: deploy
  cache:
    paths:
      - out/
  dependencies:
    - build
  script:
    - apk add python3-dev
    - pip3 install awscli
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws s3 sync out/ s3://playground-front-hori
```