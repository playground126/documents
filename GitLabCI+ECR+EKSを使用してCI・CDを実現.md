# GitLabCI + ECR + EKSを使用してCI/CDを実現

- [GitLabCI + ECR + EKSを使用してCI/CDを実現](#gitlabci--ecr--eksを使用してcicdを実現)
  - [事前準備](#事前準備)
    - [AWS CLI認証の設定](#aws-cli認証の設定)
    - [ECRの作成](#ecrの作成)
    - [EKSの環境構築](#eksの環境構築)
    - [GitLabリポジトリの環境変数を設定](#gitlabリポジトリの環境変数を設定)
  - [GitLabCIを使用してECRへDockerImageをPushする](#gitlabciを使用してecrへdockerimageをpushする)
  - [ECRのDockerイメージをもとにEKSへデプロイする](#ecrのdockerイメージをもとにeksへデプロイする)
  - [その他](#その他)

## 事前準備

### AWS CLI認証の設定  
ローカルやCIジョブ内でAWSサービスへアクセスするためにAWS CLIを設定する。
[aws configureを使用したクイック設定](https://docs.aws.amazon.com/ja_jp/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-config)

### ECRの作成
コンソールよりリポジトリを作成する。  
小ネタ：Gradleは、bootBuildImageコマンドでいい感じにDockerFileを作成してくれる。

### EKSの環境構築
※ クラスター名やリージョンの名前は適宜修正  

1. クラスター作成  
    eksctl create cluster --name demo-cluster --region ap-northeast-1 --fargate  

（以下はローカルやCIジョブ内で。デプロイや確認の際に使用するコマンド）

2. kubeconfig ファイルを作成または更新  
    クラスターへ接続するためにkubeconfigファイルを作成または更新  
    aws eks update-kubeconfig --region  ap-northeast-1 --name demo-cluster  

3. manifestファイル作成  

4. manifestファイルをもとにk8s環境を作成する  
    kubectl apply -f manifest.yml  
    
5. 作成状態確認  
    kubectl get pod  
    kubectl describe pod pod名 #詳細を確認  

6. portforwardで動作確認  
    kubectl port-forward pod名 8080:8080

7. ログ確認  
    kubectl logs pod名  

### GitLabリポジトリの環境変数を設定  

プロジェクトのサイドバー「設定」→「CI/CD」→「変数」に各環境変数を設定する。  
[アクセスキーの管理](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_CreateAccessKey)を参考に以下2つを生成し、設定する。  
- AWS_ACCESS_KEY_ID : 生成したアクセスキーID
- AWS_SECRET_ACCESS_KEY : 生成したシークレットアクセスキー
 

## GitLabCIを使用してECRへDockerImageをPushする

ディレクトリ構造は[このディレクトリ](https://gitlab.com/198632/cispringdemo/-/blob/master/.gitlab-ci.yml)を参照。  

GitLabCIにて、Dockerコマンドを無料で使用するには、kanikoを使用する必要がある。  
環境変数として以下を設定
- ECR_URI : ECRのURIを設定
- REPOSITORY_NAME : リポジトリの名称を設定

```
push_ecr:
  stage: push_ecr
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - |
      cat > /kaniko/.docker/config.json <<EOF
      {
      "credHelpers": {
                "${ECR_URI}": "ecr-login"
              }
      }
      EOF
    -  /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $ECR_URI/$REPOSITORY_NAME:$CI_COMMIT_SHA
  only:
    - master
```

ジョブの書き方は[このサイト](https://int128.hatenablog.com/entry/2019/09/25/204930)を参考にした。

## ECRのDockerイメージをもとにEKSへデプロイする

Kustomizeを使用し、manifest.yamlをもとにPullするイメージのタグを変更したファイルを作成する。その後、EKSへデプロイを実施する。  

- Kustomizeを使用し、imageタグを変更するジョブ
```
update_image_tag:
  stage: update_image_tag
  image:
    name: k8s.gcr.io/kustomize/kustomize:v3.9.2
    entrypoint: [""]
  before_script:
    - git config --global user.name "demo" # gitの初期設定
    - git config --global user.email "horihoribass@gmail.com"
  script:
    - cd $CI_PROJECT_DIR/k8s
    - kustomize edit set image $ECR_URI/$REPOSITORY_NAME=$ECR_URI/$REPOSITORY_NAME:$CI_COMMIT_SHA 
    - kustomize build # kustomizeにより修正したyamlファイルを確認できる。imageタグがコミットIDとなっている。
    - git add .
    - git commit -m "Update kustomization.yaml"
    - git remote set-url origin https://oauth2:${GITLAB_ACCESS_TOKEN}@gitlab.com/198632/cispringdemo.git
    - git push origin HEAD:$CI_COMMIT_REF_NAME
  only:
    - master
  except:
    changes:
      - k8s/**
```

gitへのPushはアクセストークンを使用して許可している。別の方法もありそう。

- EKSへデプロイを実施するジョブ
```
apply_manifests:
  stage: apply_manifests
  image: alpine/k8s:1.18.2
  script:
    - aws eks --region ap-northeast-1 update-kubeconfig --name demo-cluster
    - kubectl apply -k k8s
    - kubectl get deployment
  only:
    refs:
      - master
    changes:
      - k8s/**

```

## その他

- onlyは今後使用不可となる可能性が高い。