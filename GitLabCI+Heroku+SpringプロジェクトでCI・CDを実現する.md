# 事前準備

## ローカルからHerokuへデプロイを実施する。

CI/CDを実現する上では不要。  
基本的にHerokuの[チュートリアル](https://devcenter.heroku.com/ja/articles/getting-started-with-gradle-on-heroku)をもとに実行

### 1.Springプロジェクト作成
　　herokuへデプロイするために以下を実施。  
- application.propertiesファイルに`server.port=${PORT:8080}`を追記。[参考](https://stackoverflow.com/questions/51726181/error-r10-boot-timeout-web-process-failed-to-bind-to-port-within-90-seconds/51727894)  
- （Java11で実行する場合）system.propertiesファイルを新規作成。中身に`java.runtime.version=11`を記載。[参考](https://devcenter.heroku.com/ja/articles/customizing-the-jdk)  

### 2.Herokuアカウント作成/Heroku CLIのインストール

### 3.アプリのデプロイを実施。
プロジェクトのルートにて実行  
①　`heroku create`実行。  
②　git commit　実施  
③　`git push heroku master`にてデプロイ。  
   masterブランチ以外をデプロイしたい場合は、`git push heroku <プッシュしたいブランチ名>:master`

## HerokuでMysqlを使う方法

※事前にクレジットカードの登録が必要  

インストール  
`heroku addons:add cleardb:ignite --app <アプリの名前>`  

インストール結果の確認  
`heroku addons --app <アプリの名前>`  

ignite(無料)部分は料金プランに置き換える。  
[参考](https://qiita.com/childmarco/items/c6d0ac1a994256ab67e7)
